package GUI;

import interfacing.RMI_Interface;
import model.Medication;
import model.MedicationPlan;
import model.Patient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.SQLOutput;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class DispenserController {

    private RMI_Interface rmi_interface;
    private List<Medication> availableMedications;
    private List<String> intake_intervals;
    private DefaultTableModel model;
    private Long patientID;

    public DispenserController(DefaultTableModel model){
        this.model=model;
    }

    public void loadMedicationPlans(Patient patient, DefaultTableModel model, RMI_Interface rmiInterface){
        List<MedicationPlan> medicationPlans=rmiInterface.getMedicationPlans(patient.getID());
        availableMedications=new ArrayList<>();
        intake_intervals=new ArrayList<>();

        for(MedicationPlan plan:medicationPlans){
            availableMedications.addAll(plan.getMedication());
            intake_intervals.addAll(plan.getIntakeIntervals());
        }

        fillInMedication();
    }


    public void fillInMedication(){
        model.setRowCount(0);
        Object[] rowdata = new Object[5];
        for (int i=0;i<availableMedications.size();i++) {
            Medication medication=availableMedications.get(i);
            String intake_interval=intake_intervals.get(i);
            rowdata[0]=medication.getID();
            rowdata[1] = medication.getName();
            String side="";
            for(String s:medication.getSideEffects())
                side+=s;
            rowdata[2] = side;
            rowdata[3]=medication.getDosage();
            rowdata[4]=intake_interval;


            model.addRow(rowdata);
        }
    }

    public void isInInterval(String intakeInterval, Time time, int index){
        String[] splitted=intakeInterval.split("-");
        if(splitted[0].equals("1") && (time.getTime()>Time.valueOf("08:00:00").getTime()) && (time.getTime()<Time.valueOf("08:00:30").getTime())) {
            System.out.println("Luat dimineata");
            intake_intervals.set(index,"0-"+splitted[1]+"-"+splitted[2]);
            rmi_interface.saveMessage(availableMedications.get(index),true,"Luat dimineata",patientID);
            fillInMedication();
        }

        if(splitted[1].equals("1") && (time.getTime()>Time.valueOf("08:00:31").getTime()) && (time.getTime()<Time.valueOf("08:01:00").getTime())){
            System.out.println("Luat la amiaz");
            intake_intervals.set(index,splitted[0]+"-0-"+splitted[2]);
            rmi_interface.saveMessage(availableMedications.get(index),true,"Luat la amiaz",patientID);
            fillInMedication();
        }


        if(splitted[2].equals("1") && (time.getTime()>Time.valueOf("08:01:01").getTime()) && (time.getTime()<Time.valueOf("08:01:30").getTime())){
            System.out.println("Luat seara");
            intake_intervals.set(index,splitted[0]+"-"+splitted[1]+"-0");
            rmi_interface.saveMessage(availableMedications.get(index),true, "Luat seara",patientID);
            fillInMedication();
        }


    }

    public void validateChoice(JTextField input, Time time) {
        Long id=new Long(input.getText());
        for(Medication medication:availableMedications){
            if(medication.getID().equals(id)){
                int index=availableMedications.indexOf(medication);
                isInInterval(intake_intervals.get(index),time,index);
            }
        }

    }

    public void checkTaken(Time time){
        for(String intake:intake_intervals){
            String[] splitted=intake.split("-");
            int index=intake_intervals.indexOf(intake);
            if(splitted[0].equals("1") && (time.getTime()>Time.valueOf("08:00:30").getTime())){
                System.out.println(availableMedications.get(index).getName()+" nu a fost luat dimineata!");
                intake_intervals.set(index,"0-"+splitted[1]+"-"+splitted[2]);
                rmi_interface.saveMessage(availableMedications.get(index),false, "Nu a fost luat dimineata",patientID);
                fillInMedication();
            }

            if(splitted[1].equals("1") && (time.getTime()>Time.valueOf("08:01:01").getTime()) && (time.getTime()< Time.valueOf("08:01:30").getTime())){
                System.out.println(availableMedications.get(index).getName()+" nu a fost luat la amiaz!");
                intake_intervals.set(index,splitted[0]+"-0-"+splitted[2]);
                rmi_interface.saveMessage(availableMedications.get(index),false, "Nu a fost luat la amiaz",patientID);
                fillInMedication();
            }

            if(splitted[2].equals("1") && (time.getTime()>Time.valueOf("08:01:31").getTime())){
                System.out.println(availableMedications.get(index).getName()+" nu a fost luat seara!");
                intake_intervals.set(index,splitted[0]+"-"+splitted[1]+"-0");
                rmi_interface.saveMessage(availableMedications.get(index),false, "Nu a fost luat seara",patientID);
                fillInMedication();
            }

        }
    }

    public void setRmi_interface(RMI_Interface rmi_interface){
        this.rmi_interface=rmi_interface;
    }
    public void setPatientID(Long patientID){
        this.patientID=patientID;
    }
}
