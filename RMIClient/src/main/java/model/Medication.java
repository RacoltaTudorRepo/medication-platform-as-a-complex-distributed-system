package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Medication implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long ID;
    private String name;

    @ElementCollection(fetch=FetchType.EAGER)
    private List<String> sideEffects;
    private int dosage;

    private boolean inPlan;

    public Medication() {
    }

    public Medication(String name, List<String> sideEffects, int dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public boolean isInPlan() {
        return inPlan;
    }

    public void setInPlan(boolean inPlan) {
        this.inPlan = inPlan;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", sideEffects=" + sideEffects +
                ", dosage=" + dosage +
                '}';
    }
}
