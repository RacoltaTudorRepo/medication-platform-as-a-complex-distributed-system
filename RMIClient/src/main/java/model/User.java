package model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

import static javax.persistence.InheritanceType.SINGLE_TABLE;

@Entity
@Inheritance(strategy=SINGLE_TABLE)
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long ID;
    @Column(nullable =false)
    private String password;
    @Column(nullable =false)
    private String username;

    @Column(unique=true)
    private String name;
    private Date birthDate;
    private String gender;
    private String adress;

    @Column(name="userROLE")
    private String userROLE;

    public User() {
    }

    public User(String username, String password, String name, Date birthDate, String gender, String adress,String userROLE) {
        this.username=username;
        this.password = password;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.adress = adress;
        this.userROLE=userROLE;
    }

    public String getUserROLE() {
        return userROLE;
    }

    public void setUserROLE(String userROLE) {
        this.userROLE = userROLE;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID=" + ID +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", birthDate=" + birthDate +
                ", gender='" + gender + '\'' +
                ", adress='" + adress + '\'' +
                ", userROLE='" + userROLE + '\'' +
                '}';
    }
}
