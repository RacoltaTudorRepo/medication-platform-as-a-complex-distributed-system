package interfacing.sensor;

import model.LocalDateTimeAdapter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

@Entity
public class Activity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long activityId;

    private String name;
    private Long patientID;

    private LocalDateTime start_time;

    private LocalDateTime end_time;
    private boolean valid;

    public Activity(String name, Long patientID, LocalDateTime start_time, LocalDateTime end_time, boolean valid) {
        this.name = name;
        this.patientID = patientID;
        this.start_time = start_time;
        this.end_time = end_time;
        this.valid=valid;
    }

    public Activity(){
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPatientID() {
        return patientID;
    }

    public void setPatientID(Long patientID) {
        this.patientID = patientID;
    }

    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    public LocalDateTime getEnd_time() {
        return end_time;
    }

    public void setEnd_time(LocalDateTime end_time) {
        this.end_time = end_time;
    }

    @Override
    public String toString() {
        return "sd_project.model.Activity{" +
                "name='" + name + '\'' +
                ", patientID=" + patientID +
                ", start_time=" + start_time +
                ", end_time=" + end_time +
                '}';
    }
}

