package interfacing.sensor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import model.Caregiver;
import model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import repository.ActivityRepository;
import repository.CaregiverRepository;

import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
public class Receiver {
    @Autowired
    private Environment environment;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private CaregiverRepository caregiverRepository;

    private final static String QUEUE_NAME="activities";

    private ObjectMapper objectMapper;

    public void connectAsReceiver(){
        try{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        objectMapper=new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message=new String(delivery.getBody(), StandardCharsets.UTF_8);
            Activity activity = objectMapper.readValue(message, Activity.class);
            processReceivedActivity(activity);
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
        }

        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void processReceivedActivity(Activity activity){
        if(validateActivity(activity)){
            System.out.println(" [x] Received valid'" + activity.toString());
            activity.setValid(true);
            this.activityRepository.save(activity);
        }
        else{
            System.out.println(" [x] Received INVALID!! '" + activity.toString());
            activity.setValid(false);
            this.activityRepository.save(activity);
            List<Caregiver> caregiverList= (List<Caregiver>) caregiverRepository.findAll();
            for(Caregiver caregiver:caregiverList){
                List<Patient> patients=caregiver.getPatients();
                for(Patient patient:patients){
                    if(patient.getID().equals(activity.getPatientID())){
                        this.template.convertAndSend("/topic/socket/caregiver/"+caregiver.getID(),activity);
                        return;
                    }
                }
            }
        }

    }




    public boolean validateSleepPeriod(Activity activity){
        if(activity.getName().equals("Sleeping")){
            long hours = ChronoUnit.HOURS.between(activity.getStart_time(), activity.getEnd_time());
            if(hours>=12){
                //System.out.println("Sleeping hours: "+hours);
                return false;
            }

        }
        return true;
    }

    public boolean validateLeavingPeriod(Activity activity){
        if(activity.getName().equals("Leaving")){
            long hours = ChronoUnit.HOURS.between(activity.getStart_time(), activity.getEnd_time());
            if(hours>=12){
                //System.out.println("Leaving hours: "+hours);
                return false;
            }

        }
        return true;
    }

    public boolean validateBathroomPeriod(Activity activity){
        if(activity.getName().equals("Toileting")){
            long hours = ChronoUnit.HOURS.between(activity.getStart_time(), activity.getEnd_time());
            if(hours>=1){
                System.out.println("Bathroom hours: "+hours);
                return false;
            }

        }
        return true;
    }

    public boolean validateActivity(Activity activity){
        return (validateBathroomPeriod(activity) && validateLeavingPeriod(activity) && validateSleepPeriod(activity));
    }
}
