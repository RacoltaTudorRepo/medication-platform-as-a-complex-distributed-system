package interfacing.SOAP.Server;

import interfacing.pillDispenser.Message;
import interfacing.sensor.Activity;
import model.Patient;
import model.Recommendation;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface SOAService {

    @WebMethod
    public List<Patient> getPatients();

    @WebMethod
    public List<Activity> getActivityByPatientId(Long id);

    @WebMethod
    public List<Message> getMessageByPatientId(Long id);

    @WebMethod
    public void saveRecommendation(Recommendation rec);

}
