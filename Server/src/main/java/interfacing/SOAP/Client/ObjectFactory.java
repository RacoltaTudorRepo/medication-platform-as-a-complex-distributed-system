
package interfacing.SOAP.Client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the interfacing.SOAP.Client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPatients_QNAME = new QName("http://Server.SOAP.interfacing/", "getPatients");
    private final static QName _SaveRecommendationResponse_QNAME = new QName("http://Server.SOAP.interfacing/", "saveRecommendationResponse");
    private final static QName _GetActivityByPatientIdResponse_QNAME = new QName("http://Server.SOAP.interfacing/", "getActivityByPatientIdResponse");
    private final static QName _SaveRecommendation_QNAME = new QName("http://Server.SOAP.interfacing/", "saveRecommendation");
    private final static QName _GetActivityByPatientId_QNAME = new QName("http://Server.SOAP.interfacing/", "getActivityByPatientId");
    private final static QName _GetPatientsResponse_QNAME = new QName("http://Server.SOAP.interfacing/", "getPatientsResponse");
    private final static QName _GetMessageByPatientIdResponse_QNAME = new QName("http://Server.SOAP.interfacing/", "getMessageByPatientIdResponse");
    private final static QName _GetMessageByPatientId_QNAME = new QName("http://Server.SOAP.interfacing/", "getMessageByPatientId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: interfacing.SOAP.Client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetActivityByPatientIdResponse }
     * 
     */
    public GetActivityByPatientIdResponse createGetActivityByPatientIdResponse() {
        return new GetActivityByPatientIdResponse();
    }

    /**
     * Create an instance of {@link SaveRecommendationResponse }
     * 
     */
    public SaveRecommendationResponse createSaveRecommendationResponse() {
        return new SaveRecommendationResponse();
    }

    /**
     * Create an instance of {@link GetPatients }
     * 
     */
    public GetPatients createGetPatients() {
        return new GetPatients();
    }

    /**
     * Create an instance of {@link GetMessageByPatientId }
     * 
     */
    public GetMessageByPatientId createGetMessageByPatientId() {
        return new GetMessageByPatientId();
    }

    /**
     * Create an instance of {@link GetMessageByPatientIdResponse }
     * 
     */
    public GetMessageByPatientIdResponse createGetMessageByPatientIdResponse() {
        return new GetMessageByPatientIdResponse();
    }

    /**
     * Create an instance of {@link GetActivityByPatientId }
     * 
     */
    public GetActivityByPatientId createGetActivityByPatientId() {
        return new GetActivityByPatientId();
    }

    /**
     * Create an instance of {@link GetPatientsResponse }
     * 
     */
    public GetPatientsResponse createGetPatientsResponse() {
        return new GetPatientsResponse();
    }

    /**
     * Create an instance of {@link SaveRecommendation }
     * 
     */
    public SaveRecommendation createSaveRecommendation() {
        return new SaveRecommendation();
    }

    /**
     * Create an instance of {@link Date }
     * 
     */
    public Date createDate() {
        return new Date();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link Recommendation }
     * 
     */
    public Recommendation createRecommendation() {
        return new Recommendation();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link Patient }
     * 
     */
    public Patient createPatient() {
        return new Patient();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatients }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getPatients")
    public JAXBElement<GetPatients> createGetPatients(GetPatients value) {
        return new JAXBElement<GetPatients>(_GetPatients_QNAME, GetPatients.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveRecommendationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "saveRecommendationResponse")
    public JAXBElement<SaveRecommendationResponse> createSaveRecommendationResponse(SaveRecommendationResponse value) {
        return new JAXBElement<SaveRecommendationResponse>(_SaveRecommendationResponse_QNAME, SaveRecommendationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivityByPatientIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getActivityByPatientIdResponse")
    public JAXBElement<GetActivityByPatientIdResponse> createGetActivityByPatientIdResponse(GetActivityByPatientIdResponse value) {
        return new JAXBElement<GetActivityByPatientIdResponse>(_GetActivityByPatientIdResponse_QNAME, GetActivityByPatientIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveRecommendation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "saveRecommendation")
    public JAXBElement<SaveRecommendation> createSaveRecommendation(SaveRecommendation value) {
        return new JAXBElement<SaveRecommendation>(_SaveRecommendation_QNAME, SaveRecommendation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivityByPatientId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getActivityByPatientId")
    public JAXBElement<GetActivityByPatientId> createGetActivityByPatientId(GetActivityByPatientId value) {
        return new JAXBElement<GetActivityByPatientId>(_GetActivityByPatientId_QNAME, GetActivityByPatientId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getPatientsResponse")
    public JAXBElement<GetPatientsResponse> createGetPatientsResponse(GetPatientsResponse value) {
        return new JAXBElement<GetPatientsResponse>(_GetPatientsResponse_QNAME, GetPatientsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessageByPatientIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getMessageByPatientIdResponse")
    public JAXBElement<GetMessageByPatientIdResponse> createGetMessageByPatientIdResponse(GetMessageByPatientIdResponse value) {
        return new JAXBElement<GetMessageByPatientIdResponse>(_GetMessageByPatientIdResponse_QNAME, GetMessageByPatientIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessageByPatientId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getMessageByPatientId")
    public JAXBElement<GetMessageByPatientId> createGetMessageByPatientId(GetMessageByPatientId value) {
        return new JAXBElement<GetMessageByPatientId>(_GetMessageByPatientId_QNAME, GetMessageByPatientId.class, null, value);
    }

}
