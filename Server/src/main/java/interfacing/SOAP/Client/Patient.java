
package interfacing.SOAP.Client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for patient complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patient">
 *   &lt;complexContent>
 *     &lt;extension base="{http://Server.SOAP.interfacing/}user">
 *       &lt;sequence>
 *         &lt;element name="cared" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="medicalRecord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patient", propOrder = {
    "cared",
    "medicalRecord"
})
public class Patient
    extends User
{

    protected boolean cared;
    protected String medicalRecord;

    /**
     * Gets the value of the cared property.
     * 
     */
    public boolean isCared() {
        return cared;
    }

    /**
     * Sets the value of the cared property.
     * 
     */
    public void setCared(boolean value) {
        this.cared = value;
    }

    /**
     * Gets the value of the medicalRecord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalRecord() {
        return medicalRecord;
    }

    /**
     * Sets the value of the medicalRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalRecord(String value) {
        this.medicalRecord = value;
    }

}
