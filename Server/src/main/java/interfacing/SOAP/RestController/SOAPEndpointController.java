package interfacing.SOAP.RestController;

import interfacing.SOAP.Client.*;
import interfacing.SOAP.Client.Recommendation;
import interfacing.SOAP.ClientCaregiver.*;
import interfacing.SOAP.Server.SOAServiceCaregiverImpl;
import interfacing.SOAP.Server.SOAServiceImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Endpoint;
import java.net.URL;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("backend/soap")
public class SOAPEndpointController {

    private SOAService service;
    private SOAServiceCaregiver serviceCaregiver;

    @GetMapping("/getPatients")
    public List<Patient> getPatients(){
        return service.getPatients();
    }

    @PostMapping("/activities")
    public List<Activity> getActivitiesByPatientId(@RequestBody long id){
        return service.getActivityByPatientId(id);
    }

    @PostMapping("/messages")
    public List<Message> getMessagesByPatientID(@RequestBody long id){
        return service.getMessageByPatientId(id);
    }

    @PutMapping("/recom")
    public void saveRecommandation(@RequestBody Recommendation recommendation){
        service.saveRecommendation(recommendation);
    }

    @GetMapping("/recom")
    public List<interfacing.SOAP.ClientCaregiver.Recommendation> getRecommandations(){
        return serviceCaregiver.getRecommendations();
    }



    @Bean
    CommandLineRunner init1(SOAServiceImpl soaService, SOAServiceCaregiverImpl soaServiceCaregiver) throws Exception{
        return args->{
            Endpoint.publish("http://localhost:8999/soaservice",soaService);
            Endpoint.publish("http://localhost:8999/soaserviceCaregiver",soaServiceCaregiver);

            URL url=new URL("http://localhost:8999/soaservice");
            SOAServiceImplService soaServiceImplService=new SOAServiceImplService(url);
            service=soaServiceImplService.getSOAServiceImplPort();

            URL urlCaregiver=new URL("http://localhost:8999/soaserviceCaregiver");
            SOAServiceCaregiverImplService soaServiceCaregiverImplService=new SOAServiceCaregiverImplService(urlCaregiver);
            serviceCaregiver= soaServiceCaregiverImplService.getSOAServiceCaregiverImplPort();
        };
    }
}
