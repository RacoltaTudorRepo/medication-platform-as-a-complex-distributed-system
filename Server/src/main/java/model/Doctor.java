package model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@DiscriminatorColumn(name="DOCTOR")
public class Doctor extends User{

    public Doctor() {
    }

    public Doctor(String username, String password, String name, Date birthDate, String gender, String adress) {
        super(username,password, name, birthDate, gender, adress,"Doctor");
    }

}
