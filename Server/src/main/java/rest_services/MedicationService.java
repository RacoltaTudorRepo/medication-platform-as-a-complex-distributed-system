package rest_services;

import model.Medication;
import model.MedicationPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import repository.MedicationPlanRepository;
import repository.MedicationRepository;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("backend/medication")
public class MedicationService {
    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @PostMapping("/createMedication")
    public void createMedication(@RequestBody Medication medication){
        medicationRepository.save(medication);
    }

    @GetMapping("/getMedications")
    public List<Medication> getMedications(){
        return medicationRepository.findAll();
    }

    @PostMapping("/createMedicationPlan")
    public void createMedicationPlans(@RequestBody MedicationPlan medicationPlan){
        List<Medication> medications=medicationPlan.getMedication();
        for(Medication med:medications)
            medicationRepository.save(med);
        medicationPlanRepository.save(medicationPlan);
    }

    @PostMapping("/getMedicationPlansForPatient")
    public List<MedicationPlan> getMedicationPlansForPatient(@RequestBody Long patientID){
        return medicationPlanRepository.findAllByPatientID(patientID);
    }
}
