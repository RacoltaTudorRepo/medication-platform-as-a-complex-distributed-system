package rest_services;

import model.Credential;
import model.Doctor;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import repository.UserRepository;
import service.SessionManager;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("backend/login")
public class LoginService {
	@Autowired
    private SessionManager sessionManager;

	@Autowired
	private UserRepository userRepository;

	@PostMapping("/check")
	long checkCredentials(@RequestBody Credential credential) {
	    if(sessionManager.isLoggedIn(credential.getUsername())){
	        return 0;
        }
		System.out.println("Checking user");
        System.out.println(credential.getUsername());
		Iterable<User> users=userRepository.findAll();
		for(User user:users) {
			if(user.getUsername().equals(credential.getUsername()) && user.getPassword().equals(credential.getPassword())){
			    //sessionManager.addUser(credential.getUsername());
			    if(user.getUserROLE().equals("Doctor"))
			        return 1;
				if(user.getUserROLE().equals("Caregiver"))
					return 2;
				if(user.getUserROLE().equals("Patient"))
					return 3;
            }


		}
		return -1;
	}

	@PostMapping("/addDoctor")
	public void addUser(@RequestBody Doctor doctor){
		System.out.println(((User)doctor).toString());
		userRepository.save(doctor);
	}

    @PostMapping("/logout")
    void logoutUser(@RequestBody String username){
        sessionManager.removeUser(username);
    }
}
