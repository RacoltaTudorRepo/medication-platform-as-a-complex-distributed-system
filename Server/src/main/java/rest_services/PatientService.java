package rest_services;

import model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import repository.PatientRepository;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("backend/patient")
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;

    @PostMapping("/getPatient")
    public Patient getPatient(@RequestBody String name){
        return patientRepository.findPatientByUsername(name);
    }
}
