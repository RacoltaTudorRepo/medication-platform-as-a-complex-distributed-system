package rest_services;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import repository.*;

import java.util.Collection;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("backend/doctor")
public class DoctorService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private DoctorRepository doctorRepository;


    @PostMapping("/getDoctorByUsername")
    public Doctor getDoctorByUsername(@RequestBody String username){
        return doctorRepository.getDoctorByUsername(username);
    }

    @GetMapping("/getPatients")
    public Iterable<Patient> getPatients(){
        System.out.println("Received request");
        return patientRepository.findAll();
    }

    @GetMapping("/getCaregivers")
    public Iterable<Caregiver> getCaregivers(){
        System.out.println("Received request");
        return caregiverRepository.findAll();
    }

    @GetMapping("/getMedication")
    public Collection<Medication> getMedication(){
        System.out.println("Received request");
        return medicationRepository.findAll();
    }

    @PostMapping("/createPatient")
    public void createPatient(@RequestBody Patient patient){
        userRepository.save(patient);
    }

    @PostMapping("/createCaregiver")
    public void createCaregiver(@RequestBody Caregiver caregiver){
        System.out.println("Saving user");
        for(Patient p:caregiver.getPatients())
        {
            p.setCared(true);
            userRepository.save(p);
        }
        userRepository.save(caregiver);
    }

    @PostMapping ("/updatePatient")
    public void updateUser(@RequestBody Patient patient){
        patientRepository.save(patient);
    }

    @PostMapping("/uncarePatients")
    public void uncarePatients(@RequestBody List<Patient> uncared){
        for(Patient p:uncared){
            System.out.println(p.toString());
            p.setCared(false);
            patientRepository.save(p);
        }
    }

    @PostMapping ("/updateCaregiver")
    public void updateCaregiver(@RequestBody Caregiver caregiver){

        for(Patient p:caregiver.getPatients())
        {
            p.setCared(true);
            patientRepository.save(p);
        }
        caregiverRepository.save(caregiver);
    }

    @PostMapping("/deleteCaregiver")
    public void deleteCaregiver(@RequestBody Caregiver user){
        List<Patient> patientsWithCaregiver=patientRepository.findAllBycaregiver_id(user.getID());
        for(Patient p:patientsWithCaregiver){
            patientRepository.save(p);
        }
        userRepository.delete(user);
    }

    @PostMapping("/deletePatient")
    public void deletePatient(@RequestBody Patient user){
        userRepository.delete(user);
    }

}
