import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { UserLogin } from './login/user-login';
import {DoctorPageComponent} from "./doctor-page/doctor-page.component";
import { UserFormComponent } from './doctor-form/user-form.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CreateUserComponent } from './create-user/create-user.component';
import { CaregiverPageComponent } from './caregiver-page/caregiver-page.component';
import { CreateMedicationPlanPageComponent } from './create-medication-plan-page/create-medication-plan-page.component';
import { PatientPageComponent } from './patient-page/patient-page.component';
import {AuthGuardService} from "./service/auth-guard.service";
import { UserModifyComponent } from './user-modify/user-modify.component';
import { PatientActivitiesComponent } from './patient-activities/patient-activities.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: UserLogin},
      {path: 'doctorPage', component: DoctorPageComponent, canActivate:[AuthGuardService]},
      {path: 'adduser', component: UserFormComponent},
      {path: 'doctorPage/createUser', component: CreateUserComponent, canActivate:[AuthGuardService]},
      {path: 'caregiverPage', component: CaregiverPageComponent, canActivate:[AuthGuardService]},
      {path: 'patientPage', component: PatientPageComponent, canActivate:[AuthGuardService]},
      {path: 'doctorPage/createMedicationPlan', component: CreateMedicationPlanPageComponent, canActivate:[AuthGuardService]},
      {path: 'doctorPage/modifyUser',component:UserModifyComponent, canActivate:[AuthGuardService]},
      {path:'doctorPage/patientActivities',component:PatientActivitiesComponent,canActivate:[AuthGuardService]}
    ]),
    FormsModule,
      NgbModule
  ],
  declarations: [
    AppComponent,
    UserLogin,
    DoctorPageComponent,
    UserFormComponent,
    CreateUserComponent,
    CaregiverPageComponent,
    CreateMedicationPlanPageComponent,
    PatientPageComponent,
    UserModifyComponent,
    PatientActivitiesComponent,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
cn be found in the LICENSE file at http://angular.io/license
*/
