import { Component, OnInit } from '@angular/core';
import {Activity} from "../model/Activity";
import {Router} from "@angular/router";
import {UserService} from "../service/user-service";
import {toNumbers} from "@angular/compiler-cli/src/diagnostics/typescript_version";
import {Message} from "../model/Message";
import {Recommendation} from "../model/Recommendation";
import * as CanvasJS from '../canvasjs.min';

@Component({
  selector: 'app-patient-activities',
  templateUrl: './patient-activities.component.html',
  styleUrls: ['./patient-activities.component.css']
})
export class PatientActivitiesComponent implements OnInit {
  activities:Activity[];
  messages:Message[];
  recs:string[];
  chart:CanvasJS.Chart;
  activitiesMinutes:any[];

  constructor(private router:Router,private userService:UserService) {
    this.recs=[];
  }

  ngOnInit() {
    let id=localStorage.getItem("patient_id_activities");
    this.userService.findActivitiesForPatient(Number.parseInt(id)).subscribe(data=>{
      this.activities=data;
      this.activitiesMinutes=[];

      for(let activity of this.activities){
        let startDate=new Date(activity.startTime);
        let endDate=new Date(activity.endTime);
        let secs:number=(endDate.getTime()-startDate.getTime())/1000;
        secs /= 60;
        let mins=Math.abs(Math.round(secs));
        this.activitiesMinutes.push({y:mins,label:activity.name })
      }

      let map=[];
      for(let object of this.activitiesMinutes){
        let found=false;
        for(let obj of map)
          if(obj.label===object.label){
            obj.y+=object.y;
            found=true;
          }
        if(found===false)
          map.push(object);
      }

      this.createChart(map)
    }).add(()=>{
     this.userService.findMessagesForPatient(Number.parseInt(id)).subscribe(data=>{
       this.messages=data;
     })
    });
  }

  createChart(data:any){
    this.chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2", // "light1", "light2", "dark1", "dark2"
      exportEnabled: true,
      animationEnabled: true,
      title: {
        text: "Activity chart"
      },
      data: [{
        type: "pie",
        showInLegend: false,
        toolTipContent: "<b>{label}</b>:{y} mins (#percent%)",
        indexLabel: "{label} - #percent%",
        dataPoints: data
      }]
    });
    this.chart.render();

  }

  confirmRecommendation(i:number){
    let recommandation:Recommendation=new Recommendation();
    recommandation.description=this.recs[i];
    recommandation.patientID=Number.parseInt(localStorage.getItem("patient_id_activities"));
    console.log(recommandation)
    this.userService.persistRecommandation(recommandation).subscribe();
  }

}
