import { Component, OnInit } from '@angular/core';
import {Patient} from "../model/Patient";
import {UserService} from "../service/user-service";
import {LoginService} from "../service/login.service";
import {Caregiver} from "../model/Caregiver";
import {HttpHeaders} from "@angular/common/http";
import SockJs from "sockjs-client";
import Stomp from "stompjs";
import {Recommendation} from "../model/Recommendation";

@Component({
  selector: 'app-caregiver-page',
  templateUrl: './caregiver-page.component.html',
  styleUrls: ['./caregiver-page.component.css']
})
export class CaregiverPageComponent implements OnInit {
  patientsWithRec:any[];
  patients:Patient[];
  caregiver:Caregiver;
  headers:HttpHeaders;
  constructor(private userService:UserService) {
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","caregiver");
  }

  ngOnInit() {
    this.patientsWithRec=[];
    this.caregiver = JSON.parse(localStorage.getItem('user'));
    this.connect(this.caregiver.id);
    this.userService.getPatientsForCaregiver(this.caregiver.id, this.headers).subscribe(data => {
      this.patients = data;
    }).add(()=>{
      let recommendations:Recommendation[]=[];
      this.userService.findRecommendationsForPatient().subscribe(data=>{
        recommendations=data;
      }).add(()=>{
          this.buildArray(recommendations);
          console.log(this.patientsWithRec)
      })
    });

  }

  buildArray(recommendations:Recommendation[]){
    for(let patient of this.patients){
      let reccs:Recommendation[]=[];
      for(let rec of recommendations)
        if(rec.patientID==patient.id)
          reccs.push(rec);
      this.patientsWithRec.push({patient,reccs})
    }
  }


  connect(caregiverId:number){
    console.log("Caregiver id:"+this.caregiver.id);
    let socket = new SockJs(`https://localhost:8080/socket`,[],{});
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
      stompClient.subscribe('/topic/socket/caregiver/'+caregiverId, function (greeting) {
        //showGreeting(JSON.parse(greeting.body).name);
        console.log(JSON.parse(greeting.body));
      });
    });
  }

}
