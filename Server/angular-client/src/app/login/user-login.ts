import {Component, Input, OnInit} from '@angular/core';
import {LoginService} from "../service/login.service";
import {Credential} from "../model/Credential";
import { Router } from '@angular/router';
import {User} from "../model/User";
import {UserService} from "../service/user-service";
import {AuthGuardService} from "../service/auth-guard.service";
import {Caregiver} from "../model/Caregiver";
import {Patient} from "../model/Patient";
import {Doctor} from "../model/Doctor";
import {HttpHeaders} from "@angular/common/http";
import SockJs from "sockjs-client";
import Stomp from "stompjs";

@Component({
  selector: 'app-product-list',
  templateUrl: './user-login.html',
  styleUrls: ['./user-login.css']
})
export class UserLogin implements OnInit{
  credentials:Credential;
  newUser:User;
  id:number;
  headers:HttpHeaders;

  constructor(
      private loginService:LoginService,
      private router: Router,
      private userService:UserService){
    this.credentials=new Credential();
    this.newUser=new User();
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","doctor");
  }

  ngOnInit(): void {
    localStorage.removeItem("user_guard");
    this.disconect();
  }

  onLoginSubmit(){
   this.loginService.validateCredentials(this.credentials).subscribe(result=>{
     if(result==1) {
       let doctor:Doctor;
       this.userService.getDoctor(this.credentials.name,this.headers).subscribe(data=>{
         console.log(data);
         doctor=data;
       }).add(()=>{
         console.log(doctor);
         localStorage.setItem("user_guard",JSON.stringify(doctor));
         this.router.navigateByUrl('/doctorPage');
       })
     }
     if(result==2) {
       let caregiver:Caregiver;
       this.headers=this.headers.set("role","caregiver");
       this.userService.getCaregiver(this.credentials.name,this.headers).subscribe(data=> {
         caregiver=data;
       }).add(()=>{
         localStorage.setItem("user_guard",JSON.stringify(caregiver));
         localStorage.setItem("user",JSON.stringify(caregiver));
         this.router.navigate(['/caregiverPage']);
       });
     }

     if(result==3){
       let patient:Patient;
       this.headers=this.headers.set("role","patient");
       this.userService.getPatient(this.credentials.name,this.headers).subscribe(data=> {
         patient=data;
       }).add(()=>{
         localStorage.setItem("user_guard",JSON.stringify(patient));
         localStorage.setItem("user",JSON.stringify(patient));
         this.router.navigate(['/patientPage']);
       });

     }
     else if(result==-1){
       window.alert("User credentials not found in database!");
     }

   });
  }

  disconect(){
    let socket = new SockJs(`https://localhost:8080/socket`,[],{});
    let stompClient = Stomp.over(socket);
    stompClient.disconnect();
  }

}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/