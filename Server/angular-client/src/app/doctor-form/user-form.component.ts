import {Component, Input} from '@angular/core';
import {Doctor} from "../model/Doctor";
import {LoginService} from "../service/login.service";

@Component({
  selector: 'doctor-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

  doctor:Doctor;

  constructor(private loginService: LoginService) {
    this.doctor=new Doctor();
  }

  confirmUser() {
    if(this.doctor.username==undefined || this.doctor.password==undefined){
      window.alert("Please enter username and password for signup!");
      console.log("test")
      return;
    }
    this.loginService.addDoctor(this.doctor).subscribe();
  }
}