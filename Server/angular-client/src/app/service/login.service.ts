import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from "@angular/common/http";
import {Credential} from "../model/Credential";
import {Observable} from "rxjs";
import {User} from "../model/User";
import {Doctor} from "../model/Doctor";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private loginUrl:string;

  constructor(private http:HttpClient) {
    this.loginUrl="https://localhost:8080/backend/login";
  }

  public validateCredentials(credentials:Credential):Observable<number>{
    return this.http.post<number>(this.loginUrl+"/check",credentials);
  }

  public addDoctor(doctor:Doctor){
    return this.http.post(this.loginUrl+"/addDoctor",doctor);
  }
}
