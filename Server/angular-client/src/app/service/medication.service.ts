import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from "@angular/common/http";
import {User} from "../model/User";
import {Medication} from "../model/Medication";
import {MedicationPlan} from "../model/MedicationPlan";

@Injectable({
  providedIn: 'root'
})
export class MedicationService {

  medicationURL:string;
  constructor(private http:HttpClient) {
    this.medicationURL="https://localhost:8080/backend/medication"
  }

  public addMedication(medication:Medication,headers:HttpHeaders){
    return this.http.post<User>(this.medicationURL+"/createMedication",medication,{headers:headers});
  }

  public getAvailableMedications(headers:HttpHeaders){
    return this.http.get<Medication[]>(this.medicationURL+"/getMedications",{headers:headers});
  }

  public addMedicationPlan(medicationPlan:MedicationPlan,headers:HttpHeaders){
    return this.http.post<MedicationPlan>(this.medicationURL+"/createMedicationPlan",medicationPlan,{headers:headers});
  }

  public getMedicationPlansForPatient(id:number,headers:HttpHeaders){
    return this.http.post<MedicationPlan[]>(this.medicationURL+"/getMedicationPlansForPatient",id,{headers:headers});

  }

}
