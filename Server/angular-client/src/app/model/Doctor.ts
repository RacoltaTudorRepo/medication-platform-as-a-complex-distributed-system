import {User} from "./User";
import {Patient} from "./Patient";

export class Doctor extends User{
    constructor(){
        super();
        this.userROLE="Doctor";
    }
}