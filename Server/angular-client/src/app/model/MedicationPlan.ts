import {Medication} from "./Medication";

export class MedicationPlan{
    patientID:number;
    medication:Medication[];
    intakeIntervals:string[];
    period:number;
}