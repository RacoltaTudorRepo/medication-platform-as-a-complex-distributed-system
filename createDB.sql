CREATE DATABASE  IF NOT EXISTS `medical` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `medical`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: medical
-- ------------------------------------------------------
-- Server version	5.5.62

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity` (
  `activity_id` bigint(20) NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `patientid` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `valid` bit(1) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (48,'2011-11-28 15:18:11','Sleeping','2','2011-11-28 02:27:59',_binary '\0'),(49,'2011-11-28 11:23:36','Toileting','3','2011-11-28 10:21:24',_binary '\0'),(50,'2011-11-28 10:33:00','Showering','2','2011-11-28 10:25:44',_binary ''),(51,'2011-11-28 10:43:00','Breakfast','3','2011-11-28 10:34:23',_binary ''),(52,'2011-11-28 10:51:13','Grooming','3','2011-11-28 10:49:48',_binary ''),(53,'2011-11-28 13:05:07','Spare_Time/TV','3','2011-11-28 10:51:41',_binary ''),(54,'2011-11-28 15:06:31','Toileting','2','2011-11-28 13:06:04',_binary '\0'),(55,'2011-11-28 13:29:09','Leaving','2','2011-11-28 13:09:31',_binary ''),(56,'2011-11-28 14:21:40','Spare_Time/TV','3','2011-11-28 13:38:40',_binary '');
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (69),(69),(69);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication`
--

DROP TABLE IF EXISTS `medication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication` (
  `id` bigint(20) NOT NULL,
  `dosage` int(11) NOT NULL,
  `in_plan` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `medication_plan_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2v3f6kdpsh0u4qr05lyh2aey6` (`medication_plan_id`),
  CONSTRAINT `FK2v3f6kdpsh0u4qr05lyh2aey6` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication_plan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication`
--

LOCK TABLES `medication` WRITE;
/*!40000 ALTER TABLE `medication` DISABLE KEYS */;
INSERT INTO `medication` VALUES (38,200,_binary '','Paracetamol',41),(39,100,_binary '','Algocalmin',41),(40,250,_binary '','Digenzim',41);
/*!40000 ALTER TABLE `medication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_plan`
--

DROP TABLE IF EXISTS `medication_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_plan` (
  `id` bigint(20) NOT NULL,
  `patientid` bigint(20) DEFAULT NULL,
  `period` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_plan`
--

LOCK TABLES `medication_plan` WRITE;
/*!40000 ALTER TABLE `medication_plan` DISABLE KEYS */;
INSERT INTO `medication_plan` VALUES (41,2,0);
/*!40000 ALTER TABLE `medication_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_plan_intake_intervals`
--

DROP TABLE IF EXISTS `medication_plan_intake_intervals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_plan_intake_intervals` (
  `medication_plan_id` bigint(20) NOT NULL,
  `intake_intervals` varchar(255) DEFAULT NULL,
  KEY `FKj0t5dwces3f2sq6it0msdnvud` (`medication_plan_id`),
  CONSTRAINT `FKj0t5dwces3f2sq6it0msdnvud` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication_plan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_plan_intake_intervals`
--

LOCK TABLES `medication_plan_intake_intervals` WRITE;
/*!40000 ALTER TABLE `medication_plan_intake_intervals` DISABLE KEYS */;
INSERT INTO `medication_plan_intake_intervals` VALUES (41,'0-1-0'),(41,'1-0-1'),(41,'1-1-0');
/*!40000 ALTER TABLE `medication_plan_intake_intervals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_side_effects`
--

DROP TABLE IF EXISTS `medication_side_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_side_effects` (
  `medication_id` bigint(20) NOT NULL,
  `side_effects` varchar(255) DEFAULT NULL,
  KEY `FKdrjnmptedk4fml0b7agd3xlo1` (`medication_id`),
  CONSTRAINT `FKdrjnmptedk4fml0b7agd3xlo1` FOREIGN KEY (`medication_id`) REFERENCES `medication` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_side_effects`
--

LOCK TABLES `medication_side_effects` WRITE;
/*!40000 ALTER TABLE `medication_side_effects` DISABLE KEYS */;
INSERT INTO `medication_side_effects` VALUES (38,''),(39,''),(40,'');
/*!40000 ALTER TABLE `medication_side_effects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `id` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `medication_name` varchar(255) DEFAULT NULL,
  `patientid` bigint(20) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `taken` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (62,'2019-12-02','Algocalmin',2,'Luat dimineata',_binary ''),(63,'2019-12-02','Digenzim',2,'Nu a fost luat dimineata',_binary '\0'),(64,'2019-12-02','Paracetamol',2,'Nu a fost luat la amiaz',_binary '\0'),(65,'2019-12-02','Digenzim',2,'Nu a fost luat la amiaz',_binary '\0'),(66,'2019-12-02','Algocalmin',2,'Nu a fost luat seara',_binary '\0'),(67,'2019-12-02','Digenzim',2,'Luat dimineata',_binary '');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommendation`
--

DROP TABLE IF EXISTS `recommendation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recommendation` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `patientid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommendation`
--

LOCK TABLES `recommendation` WRITE;
/*!40000 ALTER TABLE `recommendation` DISABLE KEYS */;
INSERT INTO `recommendation` VALUES (57,'Ai depasit cu 3 ore durata maxima de somn',2),(68,'Prea mult la baie',2);
/*!40000 ALTER TABLE `recommendation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `dtype` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `userrole` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `is_cared` bit(1) DEFAULT NULL,
  `medical_record` varchar(255) DEFAULT NULL,
  `caregiver_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gj2fy3dcix7ph7k8684gka40c` (`name`),
  KEY `FKm0rrnxo0upcpuvvgaktd94jw5` (`caregiver_id`),
  CONSTRAINT `FKm0rrnxo0upcpuvvgaktd94jw5` FOREIGN KEY (`caregiver_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('Doctor',1,NULL,'1950-11-26','M','George','blabla','Doctor','doctor1',NULL,NULL,NULL),('Patient',2,NULL,'1961-01-19','Male','Radu','blabla','Patient','patient1',_binary '','Amigdalita',4),('Patient',3,NULL,NULL,NULL,'Viorel','blabla','Patient','patient2',_binary '','Gastrita,Ulcer',4),('Caregiver',4,NULL,'1980-03-19',NULL,'Tudor','blabla','Caregiver','caregiver1',NULL,NULL,NULL),('Doctor',10,NULL,NULL,NULL,'Doctor2','blabla','Doctor','doctor2',NULL,NULL,NULL),('Patient',11,NULL,'1970-02-28',NULL,'Ionut','blabla','Patient','patient3',_binary '','Mana rupta, fisura la craniu',4);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 10:36:50
