package sender;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import model.Activity;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Sender extends Thread{
    private final static String QUEUE_NAME="activities";

    private Channel channel;
    private ObjectMapper objectMapper;

    public Channel connectAsSender()throws Exception{
        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost("localhost");

        Connection connection = factory.newConnection();
        Channel channel= connection.createChannel();
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        return channel;W
    }

    public void sendData(Activity activity) throws Exception{
        String message=objectMapper.writeValueAsString(activity);
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
        System.out.println("Sent message--: "+message);
    }

    public ArrayList<Activity> readActivities() throws Exception{
        Path path= Paths.get(getClass().getClassLoader().getResource("Activities.txt").toURI());
        Stream<String> content= Files.lines(path);

        ArrayList<Activity> activities=new ArrayList<>();


        DateTimeFormatter format=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        content.forEach(line->{
            String[] splitted=line.split("\t\t");
            Long patientID=Long.parseLong(splitted[0]);
            LocalDateTime start=LocalDateTime.parse(splitted[1],format);
            LocalDateTime end=LocalDateTime.parse(splitted[2],format);
            String activity=splitted[3].trim();
            activities.add(new Activity(activity,patientID,start,end));
        });

        return activities;
    }


    @Override
    public void run(){
        try {
            objectMapper=new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            this.channel=connectAsSender();

            ArrayList<Activity> activities = readActivities();
            for(Activity activity:activities){
                sendData(activity);
                this.sleep(5000);
            }




        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
